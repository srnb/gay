package tf.bug.gay

import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import java.util.concurrent.TimeUnit

import com.tsunderebug.eightdown.Animation
import com.tsunderebug.eightdown.draw.asset.Image
import com.tsunderebug.eightdown.rendering.HighQualityGIFRenderingBackend
import com.tsunderebug.eightdown.transform.{Rotation, Transformable}
import javax.imageio.ImageIO
import javax.swing.JFileChooser

object Main {

  def main(args: Array[String]): Unit = {
    val im = new JFileChooser()
    im.showOpenDialog(null)
    val f = im.getSelectedFile
    val ii = ImageIO.read(f)
    val flag = ImageIO.read(new File("gay.png"))
    implicit val renderingBackend: HighQualityGIFRenderingBackend = HighQualityGIFRenderingBackend(Color.WHITE, transparent = false)
    val a = getAnimObject(ii, flag)
    a.renderTo(256, 256, new File("test.gif"), 25.0, 4, TimeUnit.SECONDS)
  }

  def getAnimObject(pfp: BufferedImage, flag: BufferedImage): Animation = {
    Animation(
      Image(
        i = flag,
        pos = _ => (0.5, 0.5),
        w = t => 1.0 + (Math.abs(Math.sin(t * Math.PI * 4)) * (Math.sqrt(2) - 1)),
        h = t => 1.0 + (Math.abs(Math.sin(t * Math.PI * 4)) * (Math.sqrt(2) - 1))
      ).transform(Rotation(
        rot = t => 2 * Math.PI * t,
        center = _ => (0.5, 0.5)
      )),
      Image(
        i = pfp,
        pos = _ => (0.5, 0.5),
        w = _ => 0.9,
        h = _ => 0.9
      ).transform((t: Transformable) => {
        (x: Double, y: Double) => {
          val ocx = x - 0.5
          val ocy = y - 0.5
          val r = Math.sqrt((ocx * ocx) + (ocy * ocy))
          if (r <= 0.4) {
            t.color(x, y)
          } else {
            _ => None
          }
        }
      }).transform(Rotation(
        rot = t => -2 * Math.PI * t,
        center = _ => (0.5, 0.5)
      ))
    )
  }

}
