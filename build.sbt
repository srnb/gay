lazy val root = (project in file(".")).settings(
  name := "gay",
  version := "0.1",
  scalaVersion := "2.12.6",
  resolvers += "jitpack" at "https://jitpack.io",
  libraryDependencies ++= Seq(
    "com.gitlab.srnb" %% "eightdown" % "1d918d9b07",
  ),
)
